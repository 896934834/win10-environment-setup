# 一、环境准备  
### 1.安装git  
  确认在本地已经安装好git工具：cmd：  **git --version** 
![输入图片说明](images/2/0.png)

  如果没有，参考：[windows安装git](https://gitee.com/896934834/win10-environment-setup/blob/master/git/1.windows%E5%AE%89%E8%A3%85git.md) 

### 2.新建仓库
  登录gitee后，点击新建仓库：  
![输入图片说明](images/2/1.png)  
  创建完成后会生成HTTPS和SSH地址，复制这里的HTTPS备用：  
![输入图片说明](images/2/2.png) 

# 二、上传项目到gitee  
### 1.进入想上传的项目的文件夹，然后右键点击 Open Git Bash here
![输入图片说明](images/2/4.png)

### 2.配置你的用户名及邮箱  
（如果还未配置，即项目文件中没有.git，则git init）  
git config --global user.name "你的用户名"    
git config --global user.email "你的邮箱"  

```
git config --global user.name "348348969"  
git config --global user.email "896934834@qq.com"
```
![输入图片说明](images/2/5.png)  

### 3.查看用户名及邮箱  
```
git config user.name 
git config user.email
```
### 4.初始化本地环境，把该项目变成可被git管理的仓库  
```
git init
```
![输入图片说明](images/2/6.png)

### 5.添加该项目下的所有文件
  git add .   （注意这里有个点） 
```
git add .
```
![输入图片说明](images/2/7.png)

### 6.使用如下命令将文件添加到仓库中去  
```
git commit -m '初始化提交'（说明信息为必填项，最好是信息有意义，便于后期理解）
```
![输入图片说明](images/2/8.png)

### 7.将本地代码库与远程代码库相关联 
git remote add origin https://gitee.com/896934834/仓库名称 
```
git remote add origin https://gitee.com/896934834/minio.git
```

### 8.强制把远程仓库的代码跟新到当前分支上面。ps:如果仓库为空这一步可以跳过  
```
git pull --rebase origin master
```

### 9.将本地代码推送到指定远程的仓库中  
```
git push -u origin master
```
![输入图片说明](images/2/10.png)  
![输入图片说明](images/2/9.png)  
上传成功后截图  
![输入图片说明](images/2/11.png)
